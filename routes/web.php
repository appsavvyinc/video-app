<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/list/{countryCode}', function ($countryCode) {

    $allowedCountryCode = ['USA','AUS','JPN','UK','IND','PAK','FRA','GER'];

    if (!in_array($countryCode, $allowedCountryCode)) {
        return response([
            'success' => false,
            'errorMessage' => 'Country Not Found'
        ]);
    }

    $lists = [
        "USA" => [
            [
                'vID' => "PEGccV-NOm8",
                'listID' => "PLx0sYbCqOb8QTF1DCJVfQrtWknZFzuoAE",
            ],
            [
                'vID' => "kPBzTxZQG5Q",
                'listID' => "PLF9eG4mzjHBh7PUx49OZ2DmHlB4FjfMau",
            ],
            [
                'vID' => "wJnBTPUQS5A",
                'listID' => "PL3oW2tjiIxvQ60uIjLdo7vrUe4ukSpbKl",
            ],
            [
                'vID' => "3o5KYSoOcG0",
                'listID' => "PLH6pfBXQXHEA1u0LGMCyedJy8kqyQJamr",
            ],
            [
                'vID' => "dqqtcXKxiXE",
                'listID' => "PL9RiXq3CWn9paBnQBSFMoFAyrXUUU29Sj",
            ]
        ],

        'AUS' => [
            [
                'vID' => "3tmd-ClpJxA",
                'listID' => "PLFgquLnL59anCntsRnELFZkHxPwZU_JM_",
            ],
            [
                'vID' => "6tlzpfSgWjY",
                'listID' => "PL_yG9ezurugzXXP24zvwj_kF0wtmUxmER",
            ],
            [
                'vID' => "FLC7jsu5y5g",
                'listID' => "PLOdmJ3f6-ePO5J1os_Aj1R6Sf0zWk61Qm",
            ],
            [
                'vID' => "3tmd-ClpJxA",
                'listID' => "PLFgquLnL59anCntsRnELFZkHxPwZU_JM_",
            ]

        ],

        'CAN' => [
            [
                'vID' => "3tmd-ClpJxA",
                'listID' => "PLFgquLnL59akuvsCHG83KKO2dpMA8uJQl",
            ],
            [
                'vID' => "cwcac7sqOnQ",
                'listID' => "PLDywHpqf315FJ157c--zAGwOUl8CFg8Pw",
            ],
            [
                'vID' => "papuvlVeZg8",
                'listID' => "PLw-VjHDlEOgvvecEeS-BC-qFhweKjZJ2I",
            ]
        ],

        'FRA' => [
            [
                'vID' => "2bjk26RwjyU",
                'listID' => "PLFgquLnL59ak5FwmTB7DRJqX3M2B1D7xI",
            ],
            [
                'vID' => "gaFBYPNrvAk",
                'listID' => "PL0o-m7mBAwLoU3e4pU3RU728ItScgQ8Vz",
            ],
            [
                'vID' => "ozv4q2ov3Mk",
                'listID' => "PLkN9IoY-hkDTz_5qmOJkFhhjSOLhYOf8a",
            ]
        ],

        'GER' => [
            [
                'vID' => "lc-cnCRhE7c",
                'listID' => "PLD7SPvDoEddZ4awWJjYEQdI10UP-9sGkO",
            ],
            [
                'vID' => "NRbD3ggyc7U",
                'listID' => "PL34bamv9dHtuYwTSnkuRscU0baAqOoBhs",
            ],
            [
                'vID' => "papuvlVeZg8",
                'listID' => "PLw-VjHDlEOgvvecEeS-BC-qFhweKjZJ2I",
            ]
        ],

        'JPN' => [
            [
                'vID' => "tKVN2mAKRI",
                'listID' => "PLFgquLnL59alxIWnf4ivu5bjPeHSlsUe9",
            ],
            [
                'vID' => "cZpmj1assiQ",
                'listID' => "PL9HG-OFFcaWI9fT04oUHW_szpzcMscYJG",
            ],
            [
                'vID' => "aeoiqFk9RAE",
                'listID' => "PLgy-uTutiQec9EarDhhSmoqNrE3_gnUKz",
            ]
        ],

        'UK' => [
            [
                'vID' => "_209r9TMB4M",
                'listID' => "PLywWGW4ILrvpqqkgKRV8jpZMaUPohQipP",
            ],
            [
                'vID' => "bx1Bh8ZvH84",
                'listID' => "PL633BED8890B76B76",
            ],
            [
                'vID' => "kdemFfbS5H0",
                'listID' => "PLG3-q8B8DlBLVW8L604yGeFnCLnFLbSzS",
            ]
        ],

        'PAK' => [
            [
                'vID' => "S-8D_M2mMh0",
                'listID' => "PL-T4hZhff4rymC3SQ0SuaeWfSk_fqBvrQ",
            ],
            [
                'vID' => "W31LlujrB08",
                'listID' => "PLA69D0908CB08C158",
            ],
            [
                'vID' => "iJt_FYXKiwI",
                'listID' => "PLsEShRAWBS7XG_Np7gA_JclsmL44byZqF",
            ],
            [
                'vID' => "oJaO4JdFWB8",
                'listID' => "PLWeQP8wvEdy1xenzx2_tyP4xQc0etcMG9",
            ],
            [
                'vID' => "d_gZTh-HrZE",
                'listID' => "PLAgwUvXDou0oW0zbpOOX9dpmniENhH0pO",
            ]
        ],

        'IND' => [
            [
                'vID' => "_iktURk0X-A",
                'listID' => "RDQMtkVm33DsMbQ",
            ],
            [
                'vID' => "UD-ALy5Z6Tg",
                'listID' => "PLVWtdc2bihyz9FYXP5gXeVekZqXHmigqD",
            ],
            [
                'vID' => "IfZ1N3Rz-G8",
                'listID' => "PL4E59E48AA0D6A230",
            ]
        ]
    ];

    return response()->json([
        'success' => true,
        'data' => $lists[$countryCode]
    ]);
    //return $countryId;
});

Route::get('/countries', function () {

    $countries = [
        [
            'countryName' => "United States Of America",
            'countryCode' => "USA",
        ],
        [
            'countryName' => "Australia",
            'countryCode' => "AUS",
        ],
        [
            'countryName' => "Japan",
            'countryCode' => "JPN",
        ],
        [
            'countryName' => "United Kingdom",
            'countryCode' => "UK",
        ],
        [
            'countryName' => "France",
            'countryCode' => "FRA",
        ],
        [
            'countryName' => "Germany",
            'countryCode' => "GER",
        ],
        [
            'countryName' => "Pakistan",
            'countryCode' => "PAK"
        ],
        [
            'countryName' => "India",
            'countryCode' => "IND"
        ]
    ];

    return response()->json([
        'success' => true,
        'data' => $countries
    ]);
});
